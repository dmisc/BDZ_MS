TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp


#CONFIG += c++14

LIBS += -lboost_system

QMAKE_CXXFLAGS += -std=c++1y -Wall -Wextra -pedantic
